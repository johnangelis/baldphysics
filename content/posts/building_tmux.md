---
title: "tmux and neovim on lxplus"
date: 2021-06-15T19:05:41+03:00
draft: false
toc: true
images:
tags:
  - linux, tmux
---

## Building tmux
Using lxplus, especially with CentOS 7 can be painful, with its old packages.
That includes tmux with the "fresh" version of 1.8


Lets download a new version of tmux
```bash
$ curl -4 -L https://github.com/tmux/tmux/releases/download/3.2/tmux-3.2.tar.gz
$ tar xzf tmux-3.2.tar.gz
```

Before building tmux, we gonna need newer versions for the `libevent` and
`ncurses`, so lets download and build them!

``` bash
$ curl -4 -L https://github.com/libevent/libevent/releases/download/release-2.1.12-stable/libevent-2.1.12-stable.tar.gz -O
$ curl -4 -L http://ftp.vim.org/ftp/gnu/ncurses/ncurses-6.2.tar.gz -O
$ tar xzf libevent-2.1.12-stable.tar.gz
$ cd libevent-2.1.12-stable
$ ./configure --prefix=$HOME/.local --disable-shared
$ make -j4
$ make install
$ cd ..
$ tar xzf ncurses-6.2.tar.gz
$ cd ncurses-6.2
$ export CPPFLAGS="-P"
$ ./configure --prefix=$HOME/.local --enable-symlinks
$ make -j4
$ make install
```

And we end up with both of these libraries installed in `$HOME/.local/bin`

Configuring and installing tmux now:
```bash
$ cd tmux-3.2
$ ./configure CFLAGS="-I$HOME/.local/include" LDFLAGS="-L$HOME/.local/lib" CPPFLAGS="-I$HOME/.local/include" --prefix=$HOME/.local
$ make -j4
$ make install
```
## Building neovim
We'll build ninja statically and copy the executable in `$HOME/.local/bin`

```bash
$ curl -4 -L https://github.com/ninja-build/ninja/archive/refs/tags/v1.10.2.tar.gz -O
$ tar xzf v1.10.2.tar.gz
$ cd ninja-1.10.2
$ LDFLAGS='-static' ./configure.py --bootstrap
$ cp ninja $HOME/.local/bin
```

Now we are ready to build neovim and of course install it unsurprisingly at
`$HOME/.local/bin`
```bash
$ git clone --depth 1 https://github.com/neovim/neovim.git
$ cd neovim
$ make CMAKE_BUILD_TYPE=RelWithDebInfo CMAKE_INSTALL_PREFIX=$HOME/.local
$ make install
```

